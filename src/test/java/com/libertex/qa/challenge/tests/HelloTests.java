package com.libertex.qa.challenge.tests;

import org.apache.http.Header;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.io.IOException;
import java.util.UUID;

import static com.libertex.qa.challenge.tests.HttpResponseWrapped.wrap;
import static com.libertex.qa.challenge.tests.OftenUsedAPI.checkClients;
import static com.libertex.qa.challenge.tests.TestContainersUtilities.awaitContainerHttpResponse;
import static com.libertex.qa.challenge.tests.TestContainersUtilities.getContainerHttpAddress;

@Testcontainers
public class HelloTests {

    @Container
    public GenericContainer appContainer =
            new GenericContainer<>("letsrokk/hello-world-challenge:latest").withExposedPorts(8080);

    @BeforeEach
    public void setUp() {
        appContainer.stop();
        appContainer.start();
        awaitContainerHttpResponse(appContainer);
    }

    @Test
    public void openWithLoggedInSessionId() throws IOException {
        // Add client
        wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/clients")
                .bodyString("{\"fullName\": \"fname1\", \"username\": \"uname1\"}", ContentType.APPLICATION_JSON)
                .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok"}
                        """);

        // Check client list
        checkClients(getContainerHttpAddress(appContainer), """
                        {"resultCode":"Ok","clients":["uname1"]}
                        """);

        // Client logs in
        var responseWrapped1 = wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/login")
                .bodyString("{\"username\": \"uname1\"}", ContentType.APPLICATION_JSON)
                .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok"}
                        """);

        Header[] sessionIdHeaders1 = responseWrapped1.getWrappedResponse().getHeaders("x-session-id");

        // Get hello
        wrap (Request.Get(getContainerHttpAddress(appContainer) + "/challenge/hello")
                .addHeader(sessionIdHeaders1[0])
                .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok","message":"Hello, fname1!"}
                        """);

    }

    @Test
    public void openWithLoggedOutSessionId() throws IOException {
        // Add client
        wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/clients")
                .bodyString("{\"fullName\": \"fname1\", \"username\": \"uname1\"}", ContentType.APPLICATION_JSON)
                .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok"}
                        """);

        // Check client list
        checkClients(getContainerHttpAddress(appContainer), """
                        {"resultCode":"Ok","clients":["uname1"]}
                        """);

        // Client logs in
        var responseWrapped1 = wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/login")
                .bodyString("{\"username\": \"uname1\"}", ContentType.APPLICATION_JSON)
                .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok"}
                        """);

        Header[] sessionIdHeaders1 = responseWrapped1.getWrappedResponse().getHeaders("x-session-id");

        // Client logs out
        wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/logout")
                .bodyString("{\"username\": \"uname1\"}", ContentType.APPLICATION_JSON)
                .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok"}
                        """);

        // Get hello
        wrap (Request.Get(getContainerHttpAddress(appContainer) + "/challenge/hello")
                .addHeader(sessionIdHeaders1[0])
                .execute().returnResponse()
        )
                .assertStatusCode(401)
                .assertJsonBody("""
                        {"resultCode":"Unauthorized"}
                        """);
    }

    @Test
    public void openWithIncorrectSessionId() throws IOException {
        // missing
        wrap (Request.Get(getContainerHttpAddress(appContainer) + "/challenge/hello")
                .execute().returnResponse()
        )
                .assertStatusCode(401)
                .assertJsonBody("""
                        {"resultCode":"Unauthorized"}
                        """);

        // empty
        wrap (Request.Get(getContainerHttpAddress(appContainer) + "/challenge/hello")
                .addHeader("x-session-id", "")
                .execute().returnResponse()
        )
                .assertStatusCode(401)
                .assertJsonBody("""
                        {"resultCode":"Unauthorized"}
                        """);

        // one incorrect
        wrap (Request.Get(getContainerHttpAddress(appContainer) + "/challenge/hello")
                .addHeader("x-session-id", UUID.randomUUID().toString())
                .execute().returnResponse()
        )
                .assertStatusCode(401)
                .assertJsonBody("""
                        {"resultCode":"Unauthorized"}
                        """);

        // two incorrect
        wrap (Request.Get(getContainerHttpAddress(appContainer) + "/challenge/hello")
                .addHeader("x-session-id", UUID.randomUUID().toString())
                .addHeader("x-session-id", UUID.randomUUID().toString())
                .execute().returnResponse()
        )
                .assertStatusCode(401)
                .assertJsonBody("""
                       {"resultCode":"Unauthorized"}
                        """);
    }
}
