package com.libertex.qa.challenge.tests;

import org.apache.http.Header;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.io.IOException;

import static com.libertex.qa.challenge.tests.HttpResponseWrapped.wrap;
import static com.libertex.qa.challenge.tests.OftenUsedAPI.checkClients;
import static com.libertex.qa.challenge.tests.TestContainersUtilities.awaitContainerHttpResponse;
import static com.libertex.qa.challenge.tests.TestContainersUtilities.getContainerHttpAddress;

@Testcontainers
public class LogoutTests {

    @Container
    public GenericContainer appContainer =
            new GenericContainer<>("letsrokk/hello-world-challenge:latest").withExposedPorts(8080);

    @BeforeEach
    public void setUp() {
        appContainer.stop();
        appContainer.start();
        awaitContainerHttpResponse(appContainer);
    }

    @Disabled
    @Test
    public void notExistingUserLogout() {
        // TODO
    }

    @Disabled
    @Test
    public void existingButNotLoggedUserLogout() {
        // TODO
    }

    @Disabled
    @Test
    public void logoutWithUsernameMissing() {
        // TODO
    }

    @Disabled
    @Test
    public void logoutAfterLogin() throws IOException {
        // Add client
        wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/clients")
                .bodyString("{\"fullName\": \"fname1\", \"username\": \"uname1\"}", ContentType.APPLICATION_JSON)
                .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok"}
                        """);

        // Check client list
        checkClients(getContainerHttpAddress(appContainer), """
                        {"resultCode":"Ok","clients":["uname1"]}
                        """);

        // Client logs in
        var responseWrapped1 = wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/login")
                .bodyString("{\"username\": \"uname1\"}", ContentType.APPLICATION_JSON)
                .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok"}
                        """);

        Header[] sessionIdHeaders1 = responseWrapped1.getWrappedResponse().getHeaders("x-session-id");

        // Client logs out
        wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/logout")
                .bodyString("{\"username\": \"uname1\"}", ContentType.APPLICATION_JSON)
                .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok"}
                        """);
    }

    @Disabled
    @Test
    public void logoutAfterLogout() throws IOException {
        // Add client
        wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/clients")
                .bodyString("{\"fullName\": \"fname1\", \"username\": \"uname1\"}", ContentType.APPLICATION_JSON)
                .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok"}
                        """);

        // Check client list
        checkClients(getContainerHttpAddress(appContainer), """
                        {"resultCode":"Ok","clients":["uname1"]}
                        """);

        // Client logs in
        var responseWrapped1 = wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/login")
                .bodyString("{\"username\": \"uname1\"}", ContentType.APPLICATION_JSON)
                .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok"}
                        """);

        Header[] sessionIdHeaders1 = responseWrapped1.getWrappedResponse().getHeaders("x-session-id");

        // Client logs out
        wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/logout")
                .bodyString("{\"username\": \"uname1\"}", ContentType.APPLICATION_JSON)
                .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok"}
                        """);

        // Client logs out again
        wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/logout")
                .bodyString("{\"username\": \"uname1\"}", ContentType.APPLICATION_JSON)
                .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok"}
                        """);
    }
}
