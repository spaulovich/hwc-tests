package com.libertex.qa.challenge.tests;

import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.io.IOException;

import static com.libertex.qa.challenge.tests.HttpResponseWrapped.wrap;
import static com.libertex.qa.challenge.tests.OftenUsedAPI.checkClients;
import static com.libertex.qa.challenge.tests.TestContainersUtilities.awaitContainerHttpResponse;
import static com.libertex.qa.challenge.tests.TestContainersUtilities.getContainerHttpAddress;

@Testcontainers
public class ClientTests {

    @Container
    public GenericContainer appContainer =
            new GenericContainer<>("letsrokk/hello-world-challenge:latest").withExposedPorts(8080);

    @BeforeEach
    public void setUp() {
        appContainer.stop();
        appContainer.start();
        awaitContainerHttpResponse(appContainer);
    }

    @Test
    public void severalClientsCreationTest() throws IOException {
        // Initially clients list is empty
        checkClients(getContainerHttpAddress(appContainer), """
                        {"resultCode":"Ok","clients":[]}
                        """);

        // Add one unique client
        wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/clients")
                .bodyString("{\"fullName\": \"fname1\", \"username\": \"uname1\"}", ContentType.APPLICATION_JSON)
                        .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok"}
                        """);

        // Check client list
        checkClients(getContainerHttpAddress(appContainer), """
                        {"resultCode":"Ok","clients":["uname1"]}
                        """);

        // Add 2nd unique client
        wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/clients")
                .bodyString("{\"fullName\": \"fname2\", \"username\": \"uname2\"}", ContentType.APPLICATION_JSON)
                .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok"}
                        """);

        // Check client list
        checkClients(getContainerHttpAddress(appContainer), """
                        {"resultCode":"Ok","clients":["uname1", "uname2"]}
                        """);

        // Add 3rd unique client
        wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/clients")
                        .bodyString("{\"fullName\": \"fname3\", \"username\": \"uname3\"}", ContentType.APPLICATION_JSON)
                        .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok"}
                        """);

        // Check client list
        checkClients(getContainerHttpAddress(appContainer), """
                        {"resultCode":"Ok","clients":["uname1", "uname2", "uname3"]}
                        """);
    }

    @Test
    public void clientCreationValidationTest() throws IOException {
        // Initially clients list is empty
        checkClients(getContainerHttpAddress(appContainer), """
                        {"resultCode":"Ok","clients":[]}
                        """);
        // username is missing
        wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/clients")
                        .bodyString("{\"fullName\": \"fname1\"}", ContentType.APPLICATION_JSON)
                        .execute().returnResponse()
        )
                .assertStatusCode(500)
                .assertJsonBody("""
                        {"resultCode":"UnexpectedError","errorMessage":"org.hibernate.id.IdentifierGenerationException: ids for this class must be manually assigned before calling save(): com.libertex.qa.challenge.model.Client"}
                        """);

        // Clients list is still empty
        checkClients(getContainerHttpAddress(appContainer), """
                        {"resultCode":"Ok","clients":[]}
                        """);

        // fullName is missing
        wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/clients")
                        .bodyString("{\"username\": \"uname1\"}", ContentType.APPLICATION_JSON)
                        .execute().returnResponse()
        )
                .assertStatusCode(500)
                .assertJsonBody("""
                        {"resultCode":"UnexpectedError","errorMessage":"org.hibernate.PropertyValueException: not-null property references a null or transient value : com.libertex.qa.challenge.model.Client.fullName"}
                        """);

        // Clients list is still empty
        checkClients(getContainerHttpAddress(appContainer), """
                        {"resultCode":"Ok","clients":[]}
                        """);

        // Empty fullName and username allowed
        wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/clients")
                        .bodyString("{\"fullName\": \"\", \"username\": \"\"}", ContentType.APPLICATION_JSON)
                        .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok"}
                        """);
        // Check clients list
        checkClients(getContainerHttpAddress(appContainer), """
                        {"resultCode":"Ok","clients":[""]}
                        """);
    }

    @Disabled
    @Test
    public void createDuplicatedClient() {
        // TODO
    }
}
