package com.libertex.qa.challenge.tests;

import org.apache.http.Header;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.shaded.com.google.common.collect.Lists;

import java.io.IOException;

import static com.libertex.qa.challenge.tests.HttpResponseWrapped.wrap;
import static com.libertex.qa.challenge.tests.OftenUsedAPI.checkClients;
import static com.libertex.qa.challenge.tests.TestContainersUtilities.awaitContainerHttpResponse;
import static com.libertex.qa.challenge.tests.TestContainersUtilities.getContainerHttpAddress;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.text.MatchesPattern.matchesPattern;

@Testcontainers
public class LoginTests {

    private static final String UUID_REGEX = "^[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}$";

    @Container
    public GenericContainer appContainer =
            new GenericContainer<>("letsrokk/hello-world-challenge:latest").withExposedPorts(8080);

    @BeforeEach
    public void setUp() {
        appContainer.stop();
        appContainer.start();
        awaitContainerHttpResponse(appContainer);
    }

    @Test
    public void severalUsersLoggedIn() throws IOException {
        // Add two unique clients
        wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/clients")
                .bodyString("{\"fullName\": \"fname1\", \"username\": \"uname1\"}", ContentType.APPLICATION_JSON)
                .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok"}
                        """);

        wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/clients")
                .bodyString("{\"fullName\": \"fname2\", \"username\": \"uname2\"}", ContentType.APPLICATION_JSON)
                .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok"}
                        """);

        // Check client list
        checkClients(getContainerHttpAddress(appContainer), """
                        {"resultCode":"Ok","clients":["uname1", "uname2"]}
                        """);

        // Client 1 logs in
        var responseWrapped1 = wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/login")
                .bodyString("{\"username\": \"uname1\"}", ContentType.APPLICATION_JSON)
                .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok"}
                        """);

        Header[] sessionIdHeaders1 = responseWrapped1.getWrappedResponse().getHeaders("x-session-id");
        assertThat(Lists.newArrayList(sessionIdHeaders1), hasSize(1));
        assertThat(sessionIdHeaders1[0].getValue(), matchesPattern(UUID_REGEX));

        // Client 2 logs in
        var responseWrapped2 = wrap (Request.Post(getContainerHttpAddress(appContainer) + "/challenge/login")
                .bodyString("{\"username\": \"uname1\"}", ContentType.APPLICATION_JSON)
                .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody("""
                        {"resultCode":"Ok"}
                        """);

        Header[] sessionIdHeaders2 = responseWrapped2.getWrappedResponse().getHeaders("x-session-id");
        assertThat(Lists.newArrayList(sessionIdHeaders2), hasSize(1));
        assertThat(sessionIdHeaders2[0].getValue(), matchesPattern(UUID_REGEX));

        // Check users have different session ids
        assertThat(sessionIdHeaders1[0].getValue(), is(not(sessionIdHeaders2[0].getValue())));
    }

    @Disabled
    @Test
    public void cannotLoginWithoutUsername() {
        // TODO
    }

    @Disabled
    @Test
    public void cannotLoginWithNotExistingUsername() {
        // TODO
    }

    @Disabled
    @Test
    public void loginAlreadyLoggedClient() {
        // TODO
    }

    @Disabled
    @Test
    public void loginAgainAfterLoggedOut() {
        // TODO
    }
}
