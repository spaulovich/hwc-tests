package com.libertex.qa.challenge.tests;

import org.apache.http.client.fluent.Request;
import org.testcontainers.containers.GenericContainer;

import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;

public final class TestContainersUtilities {

    public static String getContainerHttpAddress(GenericContainer container) {
        return String.format("http://%s:%d",
                container.getContainerIpAddress(), container.getMappedPort(8080));
    }

    public static void awaitContainerHttpResponse(GenericContainer container) {
        await().atMost(5, SECONDS).pollInterval(500, TimeUnit.MILLISECONDS).until(() ->
                200 == Request.Get(TestContainersUtilities.getContainerHttpAddress(container))
                        .execute().returnResponse().getStatusLine().getStatusCode());
    }

}
