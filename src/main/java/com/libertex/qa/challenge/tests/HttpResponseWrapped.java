package com.libertex.qa.challenge.tests;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flipkart.zjsonpatch.JsonDiff;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class HttpResponseWrapped {

    private HttpResponse wrappedResponse;

    private String responseBody;

    public HttpResponseWrapped(HttpResponse httpResponse) {
        this.wrappedResponse = httpResponse;
    }

    public static HttpResponseWrapped wrap(HttpResponse httpResponse) {
        return new HttpResponseWrapped(httpResponse);
    }

    public HttpResponse getWrappedResponse() {
        return wrappedResponse;
    }

    public int getStatusCode() {
        return wrappedResponse.getStatusLine().getStatusCode();
    }

    public String asString() {
        if (null == responseBody) {
            try {
                responseBody = EntityUtils.toString(wrappedResponse.getEntity());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        return responseBody;
    }

    public DocumentContext asJsonPathContext() {
        return JsonPath.parse(asString());
    }

    public HttpResponseWrapped assertJsonBody(String expectedJsonInAnyOrder) {
        ObjectMapper jacksonObjectMapper = new ObjectMapper();
        try {
            JsonNode expectedNode = jacksonObjectMapper.readTree(expectedJsonInAnyOrder);
            JsonNode actualNode = jacksonObjectMapper.readTree(asString());


            JsonNode patch = JsonDiff.asJson(expectedNode, actualNode);
            assertThat(patch.toString(), is("[]"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return this;
    }

    public HttpResponseWrapped assertStatusCode(int expectedStatusCode) {
        assertThat(getStatusCode(), is(expectedStatusCode));
        return this;
    }

}
