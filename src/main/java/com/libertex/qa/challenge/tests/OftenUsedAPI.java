package com.libertex.qa.challenge.tests;

import org.apache.http.client.fluent.Request;

import java.io.IOException;

import static com.libertex.qa.challenge.tests.HttpResponseWrapped.wrap;

public final class OftenUsedAPI {

    public static void checkClients(String httpAddress, String expectedJsonBody) throws IOException {
        wrap (Request.Get(httpAddress + "/challenge/clients")
                .execute().returnResponse()
        )
                .assertStatusCode(200)
                .assertJsonBody(expectedJsonBody);
    }
}
