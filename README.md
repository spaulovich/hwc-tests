# Setup
You will need to have pre-installed  
 - Java 13  
 - Maven  
 - Docker  

# Run Tests:
`mvn clean test`